import { ChangeDetectionStrategy, Component } from '@angular/core';
import {BehaviorSubject, catchError, combineLatest, EMPTY, map, Observable, startWith, Subject} from 'rxjs';

import { ProductCategory } from '../product-categories/product-category';

import { ProductService } from './product.service';
import {ProductCategoryService} from "../product-categories/product-category.service";

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListComponent {
  pageTitle = 'Product List';
  errorMessage = '';
  categories: ProductCategory[] = [];
  categorySelectedSubject = new BehaviorSubject<number>(0);
  categorySelectionAction$ = this.categorySelectedSubject.asObservable();
  constructor(private productService: ProductService, private productCategoryService: ProductCategoryService) { }

  products$ = combineLatest([
    this.productService.productsWithAdd$,
    this.categorySelectionAction$
  ]).pipe(map(
    ([products, selectedCategoryId]) => {
       return products.filter(product =>
          selectedCategoryId ? product.categoryId == selectedCategoryId : true
      )
    }
  ), catchError(err => {
    this.errorMessage = err;
    return EMPTY;
  }))


  categories$ = this.productCategoryService.productCategories$
    .pipe(
      catchError(err => {
        this.errorMessage = err;
        return EMPTY;
      })
    )

  onAdd(): void {
    this.productService.addProduct();
  }

  onSelected(categoryId: string): void {
    this.categorySelectedSubject.next(+categoryId);
  }
}
